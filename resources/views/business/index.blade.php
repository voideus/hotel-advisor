@extends('layouts.app')

@section('content')

<div class="container">
    <h2 class="mt-5">All Bussiness Page</h2>
    
    <ul>
        <div>
            @foreach ($businesses as $business)

            <li>
                <a href="/business/{{$business->id}}">
                    {{$business->business_name}}
                </a>
            </li>
                
            @endforeach
        </div>
    </ul>
</div>


@endsection
